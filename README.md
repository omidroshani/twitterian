# Twitterian
My application uses a new approach to crawl Twitter data by leveraging Twitter's unofficial GraphQL APIs. These APIs are used internally by Twitter for their website and other services, but there is no official documentation or public API for them. My approach involves reverse engineering these APIs to understand their behavior and extract data from them.

My approach is unique in that I use the GraphQL APIs directly, without relying on any existing packages or libraries. This allows me to access more data and perform more complex queries than would be possible with traditional Twitter API endpoints.

I've implemented this approach in my Twitter crawler, which can be used to extract tweets, user information, and other data from Twitter. My crawler is designed to be fast and efficient, making use of concurrency and caching to minimize the number of requests needed to extract the desired data.

Overall, my approach represents a new frontier in Twitter crawling and data extraction, and I'm excited to share it with the community.

<b>Tutorial Video:</b>
<br>
[Vimeo](https://vimeo.com/806548393)
<br>
[Google Drive](https://drive.google.com/file/d/1FbiXtAqYKOFmof_6weq6y-a0nId2W_1q/view?usp=sharing)
<br>
<br>
<b>API Docs:</b>
<br>
http://twitterian.texoom.net/api-docs/
<br>
http://twitterian.texoom.net/swagger/
<br>
<br>
<b>Admin Panel:</b>
<br>
http://twitterian.texoom.net/admin/
- Username: `admin`
- Password: `admin`
<br>

## Usage

### Endpoints

Users can access the documentation for these endpoints by clicking on the following [link](http://twitterian.texoom.net/api-docs/). The documentation provides additional information about each endpoint, including sample requests and responses, and any authentication requirements.
<br>
<br>
<b>API Docs:</b>
<br>
http://twitterian.texoom.net/api-docs/
<br>
http://twitterian.texoom.net/swagger/
<br>
<br>
accounts/

    Method: GET
    Description: Returns a list of all user accounts.
    Parameters: None

accounts/\<str:pk>/

    Method: GET
    Description: Returns information about a specific user account.
    Parameters: pk - The primary key of the account to retrieve. 

tweets/

    Method: GET
    Description: Returns a list of all tweets.
    Parameters: None

tweets/\<str:pk>/

    Method: GET
    Description: Returns information about a specific tweet.
    Parameters: pk - The primary key of the tweet to retrieve.

sentiment/thread/\<str:pk>/

    Method: GET
    Description: Returns the sentiment analysis for a thread of tweets.
    Parameters: pk - The primary key of the thread to analyze.

sentiment/account/\<str:pk>/

    Method: GET
    Description: Returns the sentiment analysis for a user's tweets.
    Parameters: pk - The primary key of the user account to analyze.

audience/\<str:pk>/

    Method: GET
    Description: Returns information about the audience of a user account.
    Parameters: pk - The primary key of the user account to retrieve audience information for.

description/account/\<str:pk>/

    Method: GET
    Description: Returns description about the user account.
    Parameters: pk - The primary key of the user account to retrieve description.


Note: Be sure to replace `<str:pk>` with the appropriate primary key value in each endpoint URL when making requests.
### Admin panel
Django is a popular web framework that provides developers with a wide range of tools and features to create web applications quickly and efficiently. One of the key benefits of Django is its built-in admin panel, which allows developers to manage and interact with their application data through an intuitive web-based interface.

The Django admin panel is a powerful tool that provides a user-friendly interface for managing data in your application. With the admin panel, you can easily create, read, update, and delete (CRUD) operations on your data without having to write any custom code. This saves developers a lot of time and effort, especially when dealing with complex data models.

The admin panel also provides an easy way to customize the look and feel of your application. You can customize the admin panel to match your branding and style, and even add custom functionality to the panel using Django's built-in plugin system.

<b>Admin Panel:</b>
<br>
http://twitterian.texoom.net/admin/
- Username: `admin`
- Password: `admin`


## Implementation
### Twitter Crawler

This approach is unique and innovative, and as far as I know, there are no other packages or libraries available on the internet that use this approach to crawl Twitter data. By using my application, you can gain access to a wealth of data that is not available through Twitter's official APIs or other existing libraries.

Note that using unofficial APIs carries some risks and may violate Twitter's terms of service. I do not condone or encourage any illegal or unethical use of my application, and I cannot be held responsible for any consequences that may arise from the use of our application.

### Sentiment Analyzer

I am a data scientist with a focus on natural language processing (NLP). I specialize in developing transformer models for NLP tasks, including sentiment analysis, text classification, and machine translation.

One of my projects is an IMDb sentiment analysis model, which can be found on Hugging Face's model hub. This model achieves state-of-the-art performance on the IMDb dataset and can accurately classify the sentiment of movie reviews as positive or negative.
[https://huggingface.co/omidroshani/imdb-sentiment-analysis](https://huggingface.co/omidroshani/imdb-sentiment-analysis)
<br>
<br>
To calculate sentiment scores for tweets, I use various pre-trained transformer models. By default, I use the "cardiffnlp/twitter-roberta-base-sentiment" model. Here's an overview of the process:

1. I start by cleaning and preprocessing the tweet text. This might involve removing URLs, usernames, and other extraneous information, as well as tokenizing the text into words and converting them to numerical representations that can be processed by the transformer models.

2. Next, I feed the preprocessed text into the transformer model, which produces a dictionary containing sentiment scores for the input text. The dictionary has the following format:
    ```json
    { "POSITIVE": 0.6, "NEGATIVE": 0.3, "NEUTRAL": 0.1 }
    ```
    The sentiment scores represent the probability of the input text being classified as positive, negative, or neutral, respectively.

3. I return the sentiment scores dictionary as the output of the sentiment analysis process. The sentiment scores represent the probability of the input text being classified as positive, negative, or neutral, respectively. I don't apply any threshold to these scores.

4. To improve the performance of the sentiment analysis process, I use a caching system to store the sentiment scores dictionary for a certain period of time after the initial request. This allows us to quickly retrieve the sentiment analysis results for the same tweet in subsequent requests, without having to re-run the sentiment analysis process each time.

Note that this is just a high-level overview of the sentiment analysis process, and there are many nuances and variations depending on the specific transformer model being used and the details of the preprocessing and postprocessing steps.

## Deployment

To run the application using Docker Compose, follow these steps:

1. Copy the `.env.sample` file to a new file called `.env`.

2. Update the `.env` file with your application's specific configuration settings (e.g. database connection details, API keys, etc.).

3. Run the following command to start the application:
    ```
    docker-compose up -d --build
    ```
This command will build the Docker containers for the application and start them in detached mode. You can view the logs for the containers with the following command:
```
docker-compose logs -f
```
Once the containers are running, you can access the application in your web browser at http://localhost:80.

Note: If you need to stop the containers, you can use the following command:
```
docker-compose down
```
This will stop and remove the containers, but it will not delete any persistent data (e.g. data stored in a database volume). If you want to delete all data and start fresh, you can use the following command instead:

```
docker-compose down --volumes
```
This will stop and remove the containers, as well as any volumes associated with the containers.