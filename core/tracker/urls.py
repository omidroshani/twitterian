from tracker.views.account import AccountListCreateAPI, AccountRetrieveDestroyAPI
from tracker.views.tweet import TweetListAPI, TweetRetrieveDestroyAPI
from tracker.views.sentiment import SentimentThreadAPI, SentimentAccountAPI
from tracker.views.audience import AudienceAPI
from tracker.views.description import DescriptionAPI
from django.urls import path

urlpatterns = [
    path('accounts/', AccountListCreateAPI.as_view(), name='accounts_list_create'),
    path('accounts/<str:pk>/', AccountRetrieveDestroyAPI.as_view(), name='accounts_retrieve_destroy'),
    path('tweets/', TweetListAPI.as_view(), name='tweets_list'),
    path('tweets/<str:pk>/', TweetRetrieveDestroyAPI.as_view(), name='tweets_retrieve_destroy'),
    path('sentiment/thread/<str:pk>/', SentimentThreadAPI.as_view(), name='sentiment_thread'),
    path('sentiment/account/<str:pk>/', SentimentAccountAPI.as_view(), name='sentiment_account'),
    path('description/account/<str:pk>/', DescriptionAPI.as_view(), name='description_account'),
    path('audience/<str:pk>/', AudienceAPI.as_view(), name='audience'),
]
