from celery import shared_task
from tracker.models import Account, Tweet
from utils.twitter import Twitter


@shared_task
def scrape_tweets():
    
    for account in Account.objects.all():
        twitter = Twitter()
        tweets = twitter.get_all_tweets(account.id)

        for tweet in tweets:

            obj, created = Tweet.objects.update_or_create(
                id=tweet['id'],
                defaults=tweet,
            )

            scrape_replies.delay(tweet['id'])


@shared_task
def scrape_replies(tweet_id):

    tweet = Tweet.objects.get(id=tweet_id)

    twitter = Twitter()

    replies = twitter.get_all_replies(tweet.id)

    for tweet in replies:

        obj, created = Tweet.objects.update_or_create(
            id=tweet['id'],
            defaults=tweet,
        )
