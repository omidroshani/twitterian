from tracker.models import Account
from django.contrib import admin
from rangefilter.filters import DateTimeRangeFilter, NumericRangeFilter
from import_export.admin import ExportActionMixin

@admin.register(Account)
class AccountAdmin(ExportActionMixin, admin.ModelAdmin):
    list_display = ('id', 'name', 'screen_name', 'protected', 'verified', 'created_at')
    search_fields = ('id', 'name', 'screen_name')
    list_filter = (
        'protected', 
        'verified', 
        ('created_at', DateTimeRangeFilter),
    )
