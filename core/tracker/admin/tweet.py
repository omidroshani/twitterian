from tracker.models import Tweet
from django.contrib import admin
from rangefilter.filters import DateTimeRangeFilter, NumericRangeFilter
from import_export.admin import ExportActionMixin
from django_json_widget.widgets import JSONEditorWidget
from django.db import models


@admin.register(Tweet)
class TweetAdmin(ExportActionMixin, admin.ModelAdmin):
    formfield_overrides = {
        models.JSONField: {'widget': JSONEditorWidget},
    }
    list_display = (
        'id',
        'user_id',
        'user_screen_name',
        'text',
        'likes_count',
        'quotes_count',
        'replies_count',
        'retweets_count',
        'views_count',
        'retweet_reference_id',
        'created_at',
        'updated_at',
    )
    search_fields = (
        'id',
        'conversation_id',
        'user_id',
        'user_name',
        'user_screen_name',
        'text',
    )
    list_filter = (
        'user_protected',
        'user_verified',
        ('created_at', DateTimeRangeFilter),
        ('likes_count', NumericRangeFilter),
        ('quotes_count', NumericRangeFilter),
        ('replies_count', NumericRangeFilter),
        ('retweets_count', NumericRangeFilter),
        ('views_count', NumericRangeFilter),
    )

    
