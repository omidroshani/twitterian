from tracker.models import Setting
from django.contrib import admin

@admin.register(Setting)
class SettingAdmin(admin.ModelAdmin):
    list_display = ('sentiment_analysis_model', 'crawler_mode', 'created_at', 'updated_at')
