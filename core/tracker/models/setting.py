from django.db import models


class Setting(models.Model):

    sentiment_model_choices = (
        ('cardiffnlp/twitter-xlm-roberta-base-sentiment', 'cardiffnlp/twitter-xlm-roberta-base-sentiment'),
        ('cardiffnlp/twitter-roberta-base-sentiment', 'cardiffnlp/twitter-roberta-base-sentiment'),
    )

    crawler_mode_choices = (
        ('tweets', 'Tweets'),
        ('tweets_replies', 'Tweets & Replies'),
        ('tweets_replies_replies', 'Tweets, Replies and Replies to the replies (Recursive)'),
    )

    sentiment_analysis_model = models.CharField(max_length=100, default='cardiffnlp/twitter-xlm-roberta-base-sentiment', choices=sentiment_model_choices)

    crawler_mode = models.CharField(max_length=100,default='tweets_replies',choices=crawler_mode_choices)

    created_at = models.DateTimeField(auto_now_add=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True)