from django.db import models
from utils.twitter import Twitter


class Account(models.Model):

    id = models.CharField(max_length=100, primary_key=True, unique=True, blank=True)
    name = models.CharField(max_length=100, blank=True)
    screen_name = models.CharField(max_length=100)
    protected = models.BooleanField(default=False)
    verified = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True)


    def save(self, *args, **kwargs):
        
        
        info = Twitter().get_user(self.screen_name)

        self.id = info['rest_id']
        self.name = info['name']
        self.verified = info['verified']
        self.protected = info['protected']

        super(Account, self).save(*args, **kwargs)