from django.db import models

class Tweet(models.Model):

    id = models.CharField(max_length=100, primary_key=True, unique=True)
    conversation_id = models.CharField(max_length=100)
    user_id = models.CharField(max_length=100)
    user_name = models.CharField(max_length=100)
    user_screen_name = models.CharField(max_length=100)
    user_protected = models.BooleanField()
    user_verified = models.BooleanField()
    user_followers_count = models.PositiveBigIntegerField()
    user_followings_count = models.PositiveBigIntegerField()
    user_statuses_count = models.PositiveBigIntegerField()
    text = models.TextField()
    likes_count = models.PositiveBigIntegerField()
    quotes_count = models.PositiveBigIntegerField()
    replies_count = models.PositiveBigIntegerField()
    retweets_count = models.PositiveBigIntegerField()
    views_count = models.PositiveBigIntegerField()
    retweet_reference_id = models.CharField(max_length=100,null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(auto_now=True, blank=True)
    raw_data = models.JSONField(null=True)