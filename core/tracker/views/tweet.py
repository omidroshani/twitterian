from tracker.serializers import TweetSerializer
from rest_framework import generics
from rest_framework.permissions import AllowAny
from tracker.models import Tweet
from django.utils.decorators import method_decorator
from drf_yasg.utils import swagger_auto_schema


@method_decorator(
    name="get",
    decorator=swagger_auto_schema(operation_id='List of tweets', tags=['Tweet']),
)
class TweetListAPI(generics.ListAPIView):
    queryset = Tweet.objects.all()
    serializer_class = TweetSerializer
    filterset_fields = (
        'id',
        'conversation_id',
        'user_id',
        'user_name',
        'user_screen_name',
        'user_protected',
        'user_verified',
        'user_followers_count',
        'user_followings_count',
        'user_statuses_count',
        'text',
        'likes_count',
        'quotes_count',
        'replies_count',
        'retweets_count',
        'views_count',
        'retweet_reference_id',
        'created_at',
        'updated_at',
    )


@method_decorator(
    name="get",
    decorator=swagger_auto_schema(operation_id='Retrieve a tweet', tags=['Tweet']),
)
@method_decorator(
    name="delete",
    decorator=swagger_auto_schema(operation_id='Delete a tweet', tags=['Tweet']),
)
class TweetRetrieveDestroyAPI(generics.RetrieveDestroyAPIView):
    queryset = Tweet.objects.all()
    serializer_class = TweetSerializer
