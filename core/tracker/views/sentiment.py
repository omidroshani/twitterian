from rest_framework.decorators import api_view
from rest_framework.response import Response
from tracker.models import Tweet
import re
from utils.sentiment_analysis import SentimentAnalysis
from diskcache import Cache
import json
from tracker.models import Setting
from django.utils.decorators import method_decorator
from drf_yasg.utils import swagger_auto_schema
from rest_framework.views import APIView

@method_decorator(
    name="get",
    decorator=swagger_auto_schema(operation_id='Get sentiment of a Thread', tags=['Sentiment']),
)
class SentimentThreadAPI(APIView):

    def get(self, request, pk):

        cache = Cache('twitterian')

        if cache.get(f'sentiment:thread:{pk}', None) is not None:
            return Response(json.loads(cache.get(f'sentiment:thread:{pk}', None)))
        
        query = Tweet.objects.filter(conversation_id=pk)

        if query.exists():

            tweets = ' '.join(query.values_list('text', flat=True))

            tweets = re.sub("(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)"," ",tweets).split()

            n = 400
            
            tweets = [tweets[i * n:(i + 1) * n] for i in range((len(tweets) + n - 1) // n )]

            sentiment = [ SentimentAnalysis(model=Setting.objects.first().sentiment_analysis_model).predict(' '.join(item)) for item in tweets ]

            neg_score, neu_score, pos_score = 0, 0, 0

            for item in sentiment:
                neg_score += item.get('NEGATIVE', 0)
                neu_score += item.get('NEUTRAL',0)
                pos_score += item.get('POSITIVE',0)

            neg_score_tmp = neg_score / (neg_score + neu_score + pos_score)
            neu_score_tmp = neu_score / (neg_score + neu_score + pos_score)
            pos_score_tmp = pos_score / (neg_score + neu_score + pos_score)

            analysis = {
                    'NEGATIVE': neg_score_tmp,
                    'NEUTRAL': neu_score_tmp,
                    'POSITIVE': pos_score_tmp,
            }

            cache.set(f'sentiment:thread:{pk}', json.dumps(analysis), expire=60)

            return Response(analysis)
        
        else:
            return Response({
                'message': 'NOT_FOUND'
            })
    
@method_decorator(
    name="get",
    decorator=swagger_auto_schema(operation_id='Get sentiment of an account', tags=['Sentiment']),
)
class SentimentAccountAPI(APIView):

    def get(self, request, pk):
    
        cache = Cache('twitterian')

        if cache.get(f'sentiment:account:{pk}', None) is not None:
            return Response(json.loads(cache.get(f'sentiment:account:{pk}', None)))

        query = Tweet.objects.filter(user_id=pk)

        if query.exists():

            tweets = ' '.join(query.values_list('text', flat=True))

            tweets = re.sub("(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)"," ",tweets).split()

            n = 400
            
            tweets = [tweets[i * n:(i + 1) * n] for i in range((len(tweets) + n - 1) // n )]

            sentiment = [ SentimentAnalysis(model=Setting.objects.first().sentiment_analysis_model).predict(' '.join(item)) for item in tweets ]

            neg_score, neu_score, pos_score = 0, 0, 0

            for item in sentiment:
                neg_score += item.get('NEGATIVE', 0)
                neu_score += item.get('NEUTRAL',0)
                pos_score += item.get('POSITIVE',0)

            neg_score_tmp = neg_score / (neg_score + neu_score + pos_score)
            neu_score_tmp = neu_score / (neg_score + neu_score + pos_score)
            pos_score_tmp = pos_score / (neg_score + neu_score + pos_score)

            analysis = {
                    'NEGATIVE': neg_score_tmp,
                    'NEUTRAL': neu_score_tmp,
                    'POSITIVE': pos_score_tmp,
            }

            cache.set(f'sentiment:account:{pk}', json.dumps(analysis), expire=60)

            return Response(analysis)
        
        else:
            return Response({
                'message': 'NOT_FOUND'
            })