from tracker.serializers import AccountSerializer
from rest_framework import generics
from rest_framework.permissions import AllowAny
from tracker.models import Account
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import SearchFilter, OrderingFilter
from django.utils.decorators import method_decorator
from drf_yasg.utils import swagger_auto_schema

@method_decorator(
    name="get",
    decorator=swagger_auto_schema(operation_id='List of accounts', tags=['Account']),
)
@method_decorator(
    name="post",
    decorator=swagger_auto_schema(operation_id='Create new account', tags=['Account']),
)
class AccountListCreateAPI(generics.ListCreateAPIView):
    queryset = Account.objects.all()
    serializer_class = AccountSerializer
    filterset_fields = '__all__'


@method_decorator(
    name="get",
    decorator=swagger_auto_schema(operation_id='Retrieve an account', tags=['Account']),
)
@method_decorator(
    name="delete",
    decorator=swagger_auto_schema(operation_id='Delete an account', tags=['Account']),
)
class AccountRetrieveDestroyAPI(generics.RetrieveDestroyAPIView):
    queryset = Account.objects.all()
    serializer_class = AccountSerializer
