from rest_framework.response import Response
from tracker.models import Tweet
from django.db.models import Count
from django.utils.decorators import method_decorator
from drf_yasg.utils import swagger_auto_schema
from rest_framework.views import APIView
from utils.chat_gpt import ChatGPT
from decouple import config
from diskcache import Cache
import json
import re

@method_decorator(
    name="get",
    decorator=swagger_auto_schema(operation_id='Get description of an account', tags=['Description']),
)
class DescriptionAPI(APIView):

    def get(self, request, pk):

        cache = Cache('twitterian')

        if cache.get(f'description:account:{pk}', None) is not None:
            return Response(json.loads(cache.get(f'description:account:{pk}', None)))
        
        tweets = " ".join(list(Tweet.objects.filter(user_id=pk).values_list("text", flat=True)))
        
        tweets = re.sub("(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)"," ",tweets).split()

        n = 800

        tweets = [tweets[i * n:(i + 1) * n] for i in range((len(tweets) + n - 1) // n )][0]

        text = f'In the list below I put list of tweets of a twitter\'s user, describe this user in two paragraph, about personality, emotions, etc. Here is the tweets of this user: \n {tweets}'

        description = ChatGPT(config('CHATGPT_API_KEY')).complete(text)

        cache.set(f'description:account:{pk}', json.dumps({'description': description}), expire=60)

        return Response({'description': description})

    
