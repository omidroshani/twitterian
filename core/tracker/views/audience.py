from rest_framework.response import Response
from tracker.models import Tweet
from django.db.models import Count
from django.utils.decorators import method_decorator
from drf_yasg.utils import swagger_auto_schema
from rest_framework.views import APIView


@method_decorator(
    name="get",
    decorator=swagger_auto_schema(operation_id='Get audience of an account', tags=['Audience']),
)
class AudienceAPI(APIView):

    def get(self, request, pk):

        conversations = Tweet.objects.filter(user_id=pk).all().values_list('conversation_id', flat=True)
        
        stats = Tweet.objects.filter(conversation_id__in=conversations) \
                    .exclude(user_id=pk) \
                    .all() \
                    .values('user_screen_name') \
                    .annotate(total_replies=Count('user_screen_name')) \
                    .order_by('-total_replies')[:10]
        
        return Response(list(stats))

    
