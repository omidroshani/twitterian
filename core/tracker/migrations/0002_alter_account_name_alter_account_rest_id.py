# Generated by Django 4.1.7 on 2023-03-06 08:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tracker', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='account',
            name='name',
            field=models.CharField(max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='account',
            name='rest_id',
            field=models.CharField(max_length=100, null=True),
        ),
    ]
