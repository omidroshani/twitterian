import json
import httpx
from dateutil.parser import parse
from urllib.parse import urlencode


class Twitter:

    def __init__(self) -> None:

        self.AUTH_TOKEN = "AAAAAAAAAAAAAAAAAAAAAPYXBAAAAAAACLXUNDekMxqa8h%2F40K4moUkGsoc%3DTYfbDKbT3jJPCEVnMYqilB28NHfOPqkca3qaAxGfsyKCs0wRbw"
        self.GUEST_TOKEN = self.get_guest_token()
        self.BASE_URL = 'https://api.twitter.com/graphql'

        # create HTTP client with browser-like user-agent:
        self.client = httpx.Client(headers={
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36",
        })

        self.headers = {
            "authority": "api.twitter.com",
            "authorization": f"Bearer {self.AUTH_TOKEN}",
            "content-type": "application/json",
            "origin": "https://twitter.com",
            "referer": "https://twitter.com/",
            "x-guest-token": self.GUEST_TOKEN,
            "x-twitter-active-user": "yes",
            "x-twitter-client-language": "en",
        }

        self.features = {
            "responsive_web_twitter_blue_verified_badge_is_enabled": True,
            "responsive_web_graphql_exclude_directive_enabled": False,
            "verified_phone_label_enabled": False,
            "responsive_web_graphql_timeline_navigation_enabled": True,
            "responsive_web_graphql_skip_user_profile_image_extensions_enabled": False,
            "tweetypie_unmention_optimization_enabled": True,
            "vibe_api_enabled": True,
            "responsive_web_edit_tweet_api_enabled": True,
            "graphql_is_translatable_rweb_tweet_is_translatable_enabled": True,
            "view_counts_everywhere_api_enabled": True,
            "longform_notetweets_consumption_enabled": True,
            "tweet_awards_web_tipping_enabled": False,
            "freedom_of_speech_not_reach_fetch_enabled": False,
            "standardized_nudges_misinfo": True,
            "tweet_with_visibility_results_prefer_gql_limited_actions_policy_enabled": False,
            "interactive_text_enabled": True,
            "responsive_web_text_conversations_enabled": False,
            "longform_notetweets_richtext_consumption_enabled": False,
            "responsive_web_enhance_cards_enabled": False
        }

    def get_guest_token(self):
        """register guest token for auth key"""
        headers = {
            "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
            "accept-encoding": "gzip",
            "accept-language": "en-US,en;q=0.5",
            "connection": "keep-alive",
            "authorization": f"Bearer {self.AUTH_TOKEN}",
        }
        result = httpx.post("https://api.twitter.com/1.1/guest/activate.json", headers=headers)
        guest_token = result.json()["guest_token"]  # e.g. '1622833653452804096'
        return guest_token

    def get_tweets(self, user_id, cursor = None):

        variables = {
            "userId": str(user_id),
            "count": 40,
            "includePromotedContent": False,
            "withQuickPromoteEligibilityTweetFields": False,
            "withSuperFollowsUserFields": True,
            "withDownvotePerspective": False,
            "withReactionsMetadata": False,
            "withReactionsPerspective": False,
            "withSuperFollowsTweetFields": True,
            "withVoice": True,
            "withV2Timeline": True
        }

        if cursor is not None:
            variables['cursor'] = cursor

        url = f'{self.BASE_URL}/73BM9FU1mPITScnhs6iXug/UserTweets?{urlencode({"variables": json.dumps(variables),"features": json.dumps(self.features)})}'

        response = self.client.get(url, headers=self.headers)

        tweets = []
        entries = []

        for i in response.json()['data']['user']['result']['timeline_v2']['timeline']['instructions']:
            if i['type'] == 'TimelineAddEntries':
                entries = i['entries']

        for item in entries:
            if item['content']['entryType'] == 'TimelineTimelineItem':
                t = item['content']['itemContent']['tweet_results']

                tweets.append({
                    'id':  t['result']['legacy']['id_str'],
                    'conversation_id': t['result']['legacy']['conversation_id_str'],
                    'user_id': t['result']['legacy']['user_id_str'],
                    'user_name': t['result']['core']['user_results']['result']['legacy']['name'],
                    'user_screen_name': t['result']['core']['user_results']['result']['legacy']['screen_name'],
                    'user_protected': t['result']['core']['user_results']['result']['legacy']['protected'],
                    'user_verified': t['result']['core']['user_results']['result']['legacy']['verified'],
                    'user_followers_count': t['result']['core']['user_results']['result']['legacy']['followers_count'],
                    'user_followings_count': t['result']['core']['user_results']['result']['legacy']['friends_count'],
                    'user_statuses_count': t['result']['core']['user_results']['result']['legacy']['statuses_count'],
                    'text': t['result']['legacy']['full_text'],
                    'likes_count': t['result']['legacy']['retweeted_status_result']['result']['legacy']['favorite_count'] if 'retweeted_status_result' in t['result']['legacy'] else t['result']['legacy']['favorite_count'],
                    'quotes_count': t['result']['legacy']['retweeted_status_result']['result']['legacy']['quote_count'] if 'retweeted_status_result' in t['result']['legacy'] else t['result']['legacy']['quote_count'],
                    'replies_count':t['result']['legacy']['retweeted_status_result']['result']['legacy']['reply_count'] if 'retweeted_status_result' in t['result']['legacy'] else t['result']['legacy']['reply_count'],
                    'retweets_count': t['result']['legacy']['retweeted_status_result']['result']['legacy']['retweet_count'] if 'retweeted_status_result' in t['result']['legacy'] else t['result']['legacy']['retweet_count'],
                    'views_count': int(t['result']['legacy']['retweeted_status_result']['result']['views'].get('count', 0)) if 'retweeted_status_result' in t['result']['legacy'] else int(t['result']['views'].get('count', 0)),
                    'retweet_reference_id': t['result']['legacy']['retweeted_status_result']['result']['rest_id'] if 'retweeted_status_result' in t['result']['legacy'] else None,
                    'created_at':  parse(t['result']['legacy']['created_at']),
                    'raw_data': t,
                })

        cursor_top = None
        for item in entries:
            if item['content']['entryType'] == 'TimelineTimelineCursor' and item['content']['cursorType'] == 'Top':
                cursor_top = item['content']['value']

        cursor_bottom = None
        for item in entries:
            if item['content']['entryType'] == 'TimelineTimelineCursor' and item['content']['cursorType'] == 'Bottom':
                cursor_bottom = item['content']['value']

        return tweets, cursor_top, cursor_bottom

    def get_replies(self, tweet_id, cursor = None):
        
        variables={
            "focalTweetId": str(tweet_id),
            "referrer": "profile" if cursor is None else 'tweet',
            "with_rux_injections": False,
            "includePromotedContent": False,
            "withCommunity": True,
            "withQuickPromoteEligibilityTweetFields": False,
            "withBirdwatchNotes": True,
            "withSuperFollowsUserFields": True,
            "withDownvotePerspective": False,
            "withReactionsMetadata": False,
            "withReactionsPerspective": False,
            "withSuperFollowsTweetFields": True,
            "withVoice": True,
            "withV2Timeline": True
        }

        if cursor is not None:
            variables['cursor'] = cursor

        url = f'{self.BASE_URL}/AD1BmaLl2uBoFR6JoQPNgg/TweetDetail?{urlencode({"variables": json.dumps(variables),"features": json.dumps(self.features)})}'

        response = self.client.get(url, headers=self.headers)


        tweets = []
        entries = []

        for i in response.json()['data']['threaded_conversation_with_injections_v2']['instructions']:
            if i['type'] == 'TimelineAddEntries':
                entries = i['entries']

        for item in entries:
            t = {}

            if item['content']['entryType'] == 'TimelineTimelineItem' and item['content']['itemContent']['itemType'] == 'TimelineTweet':
                t = item['content']['itemContent']['tweet_results']
                
            elif item['content']['entryType'] == 'TimelineTimelineModule':
                t = item['content']['items'][0]['item']['itemContent']['tweet_results']

            if t != {}:
                tweets.append({
                    'id':  t['result']['legacy']['id_str'],
                    'conversation_id': t['result']['legacy']['conversation_id_str'],
                    'user_id': t['result']['legacy']['user_id_str'],
                    'user_name': t['result']['core']['user_results']['result']['legacy']['name'],
                    'user_screen_name': t['result']['core']['user_results']['result']['legacy']['screen_name'],
                    'user_protected': t['result']['core']['user_results']['result']['legacy']['protected'],
                    'user_verified': t['result']['core']['user_results']['result']['legacy']['verified'],
                    'user_followers_count': t['result']['core']['user_results']['result']['legacy']['followers_count'],
                    'user_followings_count': t['result']['core']['user_results']['result']['legacy']['friends_count'],
                    'user_statuses_count': t['result']['core']['user_results']['result']['legacy']['statuses_count'],
                    'text': t['result']['legacy']['full_text'],
                    'likes_count': t['result']['legacy']['retweeted_status_result']['result']['legacy']['favorite_count'] if 'retweeted_status_result' in t['result']['legacy'] else t['result']['legacy']['favorite_count'],
                    'quotes_count': t['result']['legacy']['retweeted_status_result']['result']['legacy']['quote_count'] if 'retweeted_status_result' in t['result']['legacy'] else t['result']['legacy']['quote_count'],
                    'replies_count':t['result']['legacy']['retweeted_status_result']['result']['legacy']['reply_count'] if 'retweeted_status_result' in t['result']['legacy'] else t['result']['legacy']['reply_count'],
                    'retweets_count': t['result']['legacy']['retweeted_status_result']['result']['legacy']['retweet_count'] if 'retweeted_status_result' in t['result']['legacy'] else t['result']['legacy']['retweet_count'],
                    'views_count': int(t['result']['legacy']['retweeted_status_result']['result']['views'].get('count', 0)) if 'retweeted_status_result' in t['result']['legacy'] else int(t['result']['views'].get('count', 0)),
                    'retweet_reference_id': t['result']['legacy']['retweeted_status_result']['result']['rest_id'] if 'retweeted_status_result' in t['result']['legacy'] else None,
                    'created_at':  parse(t['result']['legacy']['created_at']),
                    'raw_data': t,
                })
        

        cursor_top = None
        for item in entries:
            if item['content']['entryType'] == 'TimelineTimelineItem' and item['content']['itemContent']['itemType'] == 'TimelineTimelineCursor' and item['content']['itemContent']['cursorType'] == 'Top':
                cursor_top = item['content']['itemContent']['value']
        
        cursor_bottom = None
        for item in entries:
            if item['content']['entryType'] == 'TimelineTimelineItem' and item['content']['itemContent']['itemType'] == 'TimelineTimelineCursor' and item['content']['itemContent']['cursorType'] == 'Bottom':
                cursor_bottom = item['content']['itemContent']['value']
        
        return tweets, cursor_top, cursor_bottom

    def get_user(self, username):

        variables={
            "screen_name":username,
            "withSafetyModeUserFields":True,
            "withSuperFollowsUserFields":True
        }

        features={
            "responsive_web_twitter_blue_verified_badge_is_enabled":True,
            "responsive_web_graphql_exclude_directive_enabled":False,
            "verified_phone_label_enabled":False,
            "responsive_web_graphql_skip_user_profile_image_extensions_enabled":False,
            "responsive_web_graphql_timeline_navigation_enabled":True
        }

        url = f'{self.BASE_URL}/rePnxwe9LZ51nQ7Sn_xN_A/UserByScreenName?variables={json.dumps(variables)}&features={json.dumps(features)}'

        response = self.client.get(url, headers=self.headers)

        if response.status_code == 200:
            return {
                'id': response.json()['data']['user']['result']['id'],
                'rest_id': response.json()['data']['user']['result']['rest_id'],
                'protected': response.json()['data']['user']['result']['legacy']['protected'],
                'verified': response.json()['data']['user']['result']['legacy']['verified'],
                'name': response.json()['data']['user']['result']['legacy']['name'],
            }
        else:
            return None

    def get_all_tweets(self, user_id, from_datetime = '2023-01-01T00:00+00:00'):
        
        tweets = []
        cursor_bottom = None
        cursor_top = None

        while True:

            new_tweets, cursor_top, cursor_bottom = self.get_tweets(user_id ,cursor_bottom)

            if new_tweets == []:
                break

            for item in new_tweets:
                if any(x['id'] == item['id'] for x in tweets):
                    return tweets
                elif item['created_at'] < parse(from_datetime):
                    return tweets
                else:
                    tweets.append(item)

        return tweets

    def get_all_replies(self, tweet_id):

        replies = []
        cursor_bottom = None
        cursor_top = None

        while True:

            new_replies, cursor_top, cursor_bottom = self.get_replies(tweet_id ,cursor_bottom)

            if new_replies == []:
                break

            for item in new_replies:
                if any(x['id'] == item['id'] for x in replies):
                    return replies
                else:
                    replies.append(item)

        return replies
