import requests

class SentimentAnalysis:

    def __init__(self, model) -> None:

        self.API_URL = f"https://api-inference.huggingface.co/models/{model}"
        self.headers = {"Authorization": "Bearer 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"}


    def query(self, payload):
        response = requests.post(self.API_URL, headers=self.headers, json=payload)
        return response.json()
    
    def predict(self, text):

        try:
            MAPPING = {
                'POS': 'POSITIVE',
                'POSITIVE': 'POSITIVE',
                'LABEL_2': 'POSITIVE',
                'NEG': 'NEGATIVE',
                'NEGATIVE': 'NEGATIVE',
                'LABEL_0': 'NEGATIVE',
                'NEU': 'NEUTRAL',
                'NEUTRAL': 'NEUTRAL',
                'LABEL_1': 'NEUTRAL',
                'positive': 'POSITIVE',
                'negative': 'NEGATIVE',
                'neutral': 'NEUTRAL',
            }

            result = self.query({"inputs": text,})
            print(result)

            return { MAPPING[score['label']]: score['score'] for score in result[0] }
        except:
            return {
                'POSITIVE':1,
                'NEGATIVE':1,
                'NEUTRAL':1,
            }
