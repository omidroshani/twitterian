from rest_framework.authentication import TokenAuthentication as TA


class TokenAuthentication(TA):
    keyword = 'Bearer'
