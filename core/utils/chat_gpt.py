import openai
from tracker.models import Tweet

class ChatGPT:

    def __init__(self,api_key, model = "text-davinci-003") -> None:
        self.model_engine = model
        openai.api_key = api_key

    def complete(self, text):
        completion = openai.Completion.create(
            engine=self.model_engine,
            prompt=text,
            max_tokens=1024,
            n=1,
            stop=None,
            temperature=0.5,
        )
        return completion.choices[0].text
